from utils import get_alexa_dataset, make_smaller_version_dataset
from transformers import (AdamW, OpenAIGPTDoubleHeadsModel, OpenAIGPTTokenizer,
                          GPT2DoubleHeadsModel, GPT2Tokenizer, WEIGHTS_NAME, CONFIG_NAME)
import matplotlib.pyplot as plt
import ast
#dataset_path = "/home/rohola/codes/knowledge_transformer_chatbot/data/train_dataset_bert.p"
dataset_path = "datasets/dataset_small_bert.p"
tokenizer_class = OpenAIGPTTokenizer  # cant use Autotokenizer because checkpoint could be a Path
tokenizer = tokenizer_class.from_pretrained("openai-gpt")
get_alexa_dataset(tokenizer, dataset_path, './caches/small_dataset_cache')

#make_smaller_version_dataset(dataset_path)

# lens = open("lens.txt").readlines()
# num_bins = 100
# lens = ast.literal_eval(lens[0])
# plt.hist(lens, num_bins, facecolor='blue', alpha=0.5)
# plt.show()